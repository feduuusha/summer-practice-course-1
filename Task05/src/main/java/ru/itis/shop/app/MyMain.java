package ru.itis.shop.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.shop.users.models.User;
import ru.itis.shop.users.repositories.UsersRepository;
import ru.itis.shop.users.repositories.impl.UsersRepositoryJdbcImpl;
import javax.sql.DataSource;

public class MyMain {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setUsername("fedor");
        config.setPassword("");
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/data");
        config.setMaximumPoolSize(52);
        DataSource dataSource = new HikariDataSource(config);
        UsersRepository usersRepository = new UsersRepositoryJdbcImpl(dataSource);
        usersRepository.save(new User(5, "Mihail", "mma@gmail.com", "itisTop1337"));
        System.out.println(usersRepository.findAll());
        usersRepository.update(new User(5, "Ilon", "teslaInterprice@ilon.mask", "neuroLink0"));
        System.out.println(usersRepository.findAll());
        usersRepository.delete(5);
        System.out.println(usersRepository.findAll());
        usersRepository.save(new User(1, "Robert", "robert-plaudis@mail.ru", "Platina300" ));
        System.out.println(usersRepository.findById(5));
        System.out.println(usersRepository.findById(1));
        usersRepository.delete(1);
        usersRepository.delete(1);
        System.out.println(usersRepository.findAll());

    }
}
